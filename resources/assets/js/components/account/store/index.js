import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'
import services from '../services/services.json'

const state = {
  updateButton: 'Update',
  account: {
    information: {
      name: '',
      address: '',
      timezone: 'Europe/Paris',
      rooms: 0,
      stars: 0,
      currency: 'EUR',
      checkIn: '',
      checkOut: '',
      photo_url: ''
    },
    services: services,
    websites: [''],
    notificationEmails: [
      {
        name: 'main',
        email: '',
        phone: ''
      },
      {
        name: 'mainNotification',
        email: '',
        phone: ''
      },
      {
        name: 'secondNotification',
        email: '',
        phone: ''
      }
    ]
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})


import axios from 'axios'
import _ from 'lodash'

const intlTelInputOptions = {
    autoPlaceholder: 'polite',
    allowDropdown: true,
    preferredCountries: ['fr'],
    utilsScript: '/js/intlTelInput_utils.js',
    autoHideDialCode: true,
    initialDialCode: true,
    americaMode: false,
    onlyCountries: [],
    numberType: 'MOBILE',
    separateDialCode: false,
    nationalMode: false
}

export const retriveData = ({
                                commit
                            }) => {
    axios.get(laroute.route('account.settings.information'))
        .then(function (response) {
            const data = response.data
            commit('receiveInformation', data)
            if (_.has(data, 'services')) {
                commit('receiveServices', data.services)
            }
            if (_.has(data, 'websites')) {
                commit('receiveWebsites', data.websites)
            }
            if (_.has(data, 'notificationEmails')) {
                commit('receiveNotificationEmails', data.notificationEmails)
            }
            $('.phone-number').intlTelInput(intlTelInputOptions)
        })
        .catch(function (error) {
            console.log(error)
        })
}

export const incrementNotification = ({commit}, notification) => {
    commit('incrementNotification', notification)
}

export const decrementNotification = ({commit}, notificationName) => {
    commit('decrementNotification', notificationName)
}

export const saveInformationData = ({
                                        commit, state
                                    }) => {
    commit('changeUpdateButton', true)
    axios.post(laroute.route('account.settings.save'), state.account)
        .then(function (response) {
            commit('changeUpdateButton', false)
        })
        .catch(function (error) {
            commit('changeUpdateButton', false)
            console.log(error)
        })
}

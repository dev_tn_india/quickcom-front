export const information = state => state.account.information
export const services = state => state.account.services
export const websites = state => state.account.websites
export const notifications = state => state.account.notificationEmails
export const updateButton = state => state.updateButton

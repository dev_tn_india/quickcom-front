import _ from 'lodash'

const intlTelInputOptions = {
  autoPlaceholder: 'polite',
  allowDropdown: true,
  preferredCountries: ['fr'],
  utilsScript: '/js/intlTelInput_utils.js',
  autoHideDialCode: true,
  initialDialCode: true,
  americaMode: false,
  onlyCountries: [],
  numberType: 'MOBILE',
  separateDialCode: false,
  nationalMode: false
}

export default {
  /**
   * Change update button
   *
   * @param {*} state
   * @param {boolean} status
   */
  changeUpdateButton (state, status) {
    status ? state.updateButton = 'Updating ...' : state.updateButton = 'Update'
  },

  /**
   * Receive information from state
   *
   * @param {*} state
   * @param {object} data
   */
  receiveInformation (state, data) {
    state.account.information = {
      name: data.name || '',
      address: data.address || {
        location: '',
        latitude: '',
        longitude: ''
      },
      timezone: data.timezone || 'Europe/Paris',
      rooms: data.rooms || '',
      stars: data.stars || 0,
      currency: data.currency || 'EUR',
      checkIn: data.checkIn || '',
      checkOut: data.checkOut || '',
			photo_url: data.photo_url || ''
    }
  },

  /**
   * Receive services from response
   *
   * @param {*} state
   * @param {object} services
   */
  receiveServices (state, services) {
    const newServices = []
    state.account.services.forEach((oldService, index) => {
      const serviceIndex = _.findIndex(services, function (newService) {
        return oldService.id === newService.id
      })
      serviceIndex >= 0
        ? newServices.push(services[serviceIndex])
        : newServices.push(oldService)
    })
    state.account.services = newServices
  },

  /**
   * Receive websites from response
   *
   * @param {*} state
   * @param {object} websites
   */
  receiveWebsites (state, websites) {
    state.account.websites = websites || ['']
  },

  /**
   * Receive information from response
   *
   * @param {*} state
   * @param {object} notificationEmails
   */
  receiveNotificationEmails (state, notificationEmails) {
    state.account.notificationEmails = notificationEmails || state.account.notificationEmails;
  },

  /**
   * Add a new notification proprety to notifications object
   *
   * @param {*} state
   * @param {object} notification
   */
  incrementNotification (state, notification) {
    var existedNotification = _.findIndex(state.account.notificationEmails, function (notif) {
      return notif.name === notification.name
    })
    if (existedNotification === -1) {
      state.account.notificationEmails.push(notification)
      setTimeout(function () { $('.phone-number').intlTelInput(intlTelInputOptions) }, 500)
    }
  },

  /**
   * Remove notification proprety from notifications object
   *
   * @param {*} state
   * @param {string} notificationName
   */
  decrementNotification (state, notificationName) {
    var existedNotification = _.findIndex(state.account.notificationEmails, function (notif) {
      return notif.name === notificationName
    })
    if (existedNotification !== -1) {
      state.account.notificationEmails.splice(existedNotification, 1)
    }
  }
}

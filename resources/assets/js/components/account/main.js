import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDqM4FibSXfOtbObftkwOnLTOj6_yBhTYo',
    libraries: 'places'
  }
})

import Account from './components/Account'
import store from './store'
import {
  retriveData
} from './store/actions'

new Vue({
  el: '#account',
  store,
  render: h => h(Account)
})
retriveData(store)

/**
 * Layout Components
 */
require('./components/layout/header');
require('./components/layout/leftSider');
require('./components/conversation/conversationApp');
require('./components/sent/sentApp');
require('./components/stats/stats');
require('./spark/forms/form');
require('./spark/forms/errors');
require('./spark-components/settings/settings');
require('./spark-components/settings/profile/update-profile-photo');
require('./spark-components/settings/profile/update-profile-photo-admin');

new Vue({
	el: '#quicktext-app',
});

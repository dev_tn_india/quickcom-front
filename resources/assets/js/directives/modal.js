Vue.directive('team-subscription', {
	bind(elt, bindings, vnode) {
		elt.addEventListener('click', (e) => {
			e.stopImmediatePropagation();
			const features = Spark.state.currentTeam.current_plan.features;
			const feature = features.filter((ft) => {
				return ft.code == bindings.value.code;
			});
			const featureType = feature.length > 0 ? feature[0].code : null;
			switch (featureType) {
				case 'MAX_USER':
					if (Spark.state.currentTeam.users.length >= feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'PMS':
					if (!feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'CALL_FORWARD':
					if (!feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'MAX_PHONE':
					if (Spark.state.currentTeam.phone_numbers.length >= feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'MAX_IM':
					if (Spark.state.currentTeam.im_team.length >= feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'BOT_MAX_PROVIDER':
					if (Spark.state.currentTeam.im_team.length >= feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'BOT_MAX_LANGUAGE':
					if (!feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'BOT_MAX_INTENTION':
					if (!feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
				case 'BOT':
					if (!feature[0].value) {
						vnode.context.$modal.show('dialog', {
							text: 'You are too awesome',
						});
						e.stopImmediatePropagation();
					}
					break;
			}
			return false;

		});
	},
});
Vue.directive('restrict', {
	inserted: function (elt, bindings, vnode) {
		elt.addEventListener('click', function (e) {
			// the goal is to stop the click event from traveling
			// to the "doSomething" method.
			e.stopPropagation();
			e.preventDefault();
			e.stopImmediatePropagation();
			alert('restricted');
			return true;
		});
	},
});

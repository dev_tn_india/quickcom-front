
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
window.Vuex = require('vuex');
window.VueResource = require('vue-resource');


/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

/**
* We'll register a HTTP interceptor to attach the "CSRF" header to each of
* the outgoing requests issued by this application. The CSRF middleware
* included with Laravel will automatically verify the header's value.
*/

window.axios = require('axios');

window.moment = require('moment');
window.moment_tz = require('moment-timezone');

/**
 import modal globally to prevent execution of such functionnality depending on selected plan
 */
import VModal from 'vue-js-modal';
Vue.use(VModal, { dialog: true });
require('./directives/modal');

const restrict  = require('../../../public/js/restrict').default;
window.showSubscriptionRequestModal = restrict.showSubscriptionRequestModal;

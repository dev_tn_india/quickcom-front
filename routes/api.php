<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

/**
 * API V2 Endpoints
 */

use Illuminate\Support\Facades\Route;

Route::group([ "prefix" => "v2/partner", "middleware" => ["api"], "namespace" => 'API\PartnerApi\V2' ], function() {

    Route::group(["middleware" => ["apiV2"]], function () {
        Route::post("/orders", "OrdersController@store")->name('api.store');
        Route::put("/orders/{ref_id}", "OrdersController@update")->name('api.edit');
        Route::delete("/orders/{ref_id}", "OrdersController@delete")->name('api.delete');
        Route::get("/orders/{ref_id?}", "OrdersController@get")->name('api.list');

        /**
         * Contact actions
         */
        Route::get("/contact/{id}", "OrdersController@getContact")->name('api.contact.get');
        Route::put("/contact/{id}", "OrdersController@editContact")->name('api.contact.edit');

        /**
         * Contact Conversation actions
         */
        Route::get("/contact/{id}/messages", "ConversationController@get")->name('api.contact.messages.get');

        /**
         * Accounts API
         */
        Route::resource("accounts", "AccountsController");

        /**
         * Webhooks API
         */
        Route::post('webhook', 'WebhookController@store')->name('api.webhook.store');
        Route::put('webhook', 'WebhookController@update')->name('api.webhook.update');
        Route::delete('webhook', 'WebhookController@delete')->name('api.webhook.delete');
        //Route::resource("webhook", "WebhookController", ['only' => ['store', 'update', 'delete']]);


        /**
         * LiveChat API
         */
      
    });
});

/**
 * Private API Endpoints
 */
Route::group([
    'middleware' => ['api'],
    'prefix' => 'private'
], function () {

    /**
     * Private API Open Endpoints
     */
    Route::group([
        'prefix' => 'open',
		'middleware' => ['privateApiWhiteList']
    ], function() {
        // Health Check routes
        Route::get("/health-check/db", "API\\PrivateApi\\V1\\Open\\HealthCheckController@db")->name("api.private.open.health_check.db");
        Route::get("/health-check/redis", "API\\PrivateApi\\V1\\Open\\HealthCheckController@redis")->name("api.private.open.health_check.redis");
       });

  
     /**
     * create a jwt api
     */
    Route::group(['middleware' => []], function () {
        //all routes
        Route::post('authenticate', 'API\\PrivateApi\\V1\\AuthenticationController@login');
        Route::post('password/reset', 'API\\PrivateApi\\V1\\AuthenticationController@reset_password');
        Route::group([
            'prefix' => 'secure',
            'namespace' => 'API\PrivateApi\V1\Secure'
        ], function() {
            Route::get('/token/check', 'AuthenticationController@checkToken');
            Route::get('/token/revoke', 'AuthenticationController@revokeToken');
            Route::get('/token/refresh', 'AuthenticationController@refreshToken');
            Route::get('/me', 'AuthenticationController@getAuthUser');
            Route::get('/inbox', 'ConversationController@inbox');
            Route::get('/unread-count', 'ConversationController@getUnreadCount');
            Route::get('/outbox', 'ConversationController@outbox');
            Route::get('/messages', 'ConversationController@messages');
            Route::post('/archive', 'ConversationController@archive');
            Route::delete('/delete', 'ConversationController@delete');
            Route::get('/teams/{id}/switch', 'TeamController@switchTeams');
        });
    });

    /**
     * AI stats routes
     */
});

/**
 * Public API Endpoints
 */

});

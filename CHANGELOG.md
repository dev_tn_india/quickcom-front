# Changelog

All notable changes to Quicktext App will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [4.19.0] - 2018-11-20

### Added [4.19.0]

- Add listing for booking stats by zoe
- Add private API IP Auth
- Add event listener to save Zoe's reservation in DB
- Add BE Park piolet by URL
- Add  translation widget
- Add zoe's bookings list
- Add new Year Eve Reservation intention in restaurant
- Add new Christmas Celebration intention in restaurant
- Add appId param to descktop notifications API
- Add route to restore conversation
- Add a new Azure Cosmos DB for bookings logs
- Add a new console command to initialize hasRestaurant value in 18-01 intention
- Add hotel settings to notification center view
- Add reservation stats interface

### Changed [4.19.0]

- Increase import period of Hores  API
- Change celebration dialog to custom component
- Change log saving from Mysql DB to Azure Cosmos DB 
- Change Thais Api
- Update notification center's UX
- Hundle mews API updates
- Update saving Zoe booking requests

### Fixed [4.19.0]

- Fix the filter bug in the blacklist interface
- Fix push notification
- Fix FB send Message
- Fix script related issue
- Fix browser notification
- Fix bug design of settings dashboard in admin interface
- Fix restaurant dialog
- Fix bug of unread message notification
- Fix notifications email
- Fix Zoe duplicated messages

## [4.18.0] - 2018-11-06

### Added [4.18.0]

- Add Google Tag Manager 
- Add Czech language for live chat
- Handle phone number validation exception
- Remove duplicate scheduled message  with # resId
### Changed [4.18.0]

- Update public messaging API version 

### Fixed [4.18.0]

- Fix upload avatar 
- Fix send scheduled messages 
- Fix configuration notification by real time 
- Fix Hotel receive booking.com messages for Hotel Norfolk Towers 
- Fix showing modals of permission  
- Fix show Zoe menu in impersonator  
- Fix messages in the right order  
- Fix 419 message  
- Fix url redirect for accor  
- Fix modal add member  
- Fix add tag to conversation  
- Fix delete conversation  
- Fix icon archive conversation
- Fix update date for Scheduled messages
- Fix Message Signature
- Fix validate data in create team
- Fix update page, time never of widget config
- Fix loading blacklists page
- Fix PMS import(file, web, api)
- Fix bug: delete sent conversation

## [4.17.0] - 2018-10-24

### Added [4.17.0]

- Activate automatically Zoe notification for the intention invoice
- Add collapse/expand to dialogs interface
- Add confirm modal when sending SMS message to online LC user
- Add 'Hoteliers' BE to APIs
- Add new search bar in bot dialogs interface
- Add validation form edit contact
- Add zoe middleware 
- Add free plan to accounts in partner API
- Add 'Roll in shower' config and attach it to 'Disabled guest'
- Add uid to archived msg
- Send email on account revoke  and reactivation via Booking 
- Add hotel name to all email templates
- Add prod whatfix script
- Init unit test

### Changed [4.17.0]

- Change api zenChef model config 
- Set only available countries in buy phone numbers
- Update Header text messages (instantly - few minutes)
- Update 'Response time instant text' translations
- Force unsubcribe FB page if it's already subscribed
- Migration to spark 7 and laravel 5.7
- Change style for GDS fields in travel agent dialog
- Modify reservation_time's condition to display today's bookings
- Optimize spark request
- Move html code of The side bar and header to blade view 
- update language portegeese
- optimize of archive messages method
- Update logging file
- Move phone validator function to util helper
- Fix Pms Connexion when selecting FTP Method

### Fixed [4.17.0]

- Fix archived message
- Fix send with multiple channel when conversation has Bowo channel
- Fix mapping XML files for PMS with FTP
- Fix update twilio config in redis when remove number
- Hide modal after delete phone number
- Fix convert Timestamp to Date in leftSideUserList
- Fix tags in archive
- Fix deleteing draft messages
- Fix update live chat language when hotel has no bot 
- Fix phone number config in Hores config
- Fix display actions for items in archived messages
- Retrieve notification config by userId

### Removed [4.17.0]

- Removed unused vue components dependency 
- Remove limit in getSettings response
- Remove unused API call

## [4.16.0] - 2018-09-25

### Added [4.16.0]

- Add extra fields for the brunch dialog
- Add synchronisation between checkIn dialog and account settions's config
- Add synchronisation between checkOut dialog and account settions's config
- Add synchronisation between number of rooms's dialog and account settions's config
- Add handle week internet connection's function for websocket
- Add 'Price group' to 'Trip Advisor'
- Add 'Simple Booking' config to booking APIs
- Add missed data for open api
- Add php helpers (AccountHelper, BotHelper, RedisUpdater)
- Add redirection to intended page after login
- Add 'TravelClick' config to booking APIs
- Add travel agent dialog
- Add 'Stardekk' to booking APIs
- Add 'Vertical booking' to booking APIs
- Add 'Omnibees' to booking APIs
- Add infinit scroll to archives
- Add a fix spoken languages's command
- Add 'Bookassist' to booking APIs
- Add deleted cloumn to user table
- Save and display last login date
- Add 'Witbooking' to booking APIs
- Add customOnly attribute to dialog object

### Changed [4.16.0]

- Update phone number property (PMS)
- Update Hores PMS
- Update app settings in RT (Redis) when account's data is updated
- Update number of conversation in page to 20
- Update intentions pre-selected (13-42, 18-04, 18-03, 16-03)
- Reuse components in archives page
- Disable user when it's deleted in users list (Admin)
- Disable impersonate when user is not active
- Make hores api url in 'env' file
- Update get data from twillio

### Fixed [4.16.0]

- Fix UX of the merge conversation's modal
- Fix notification problems
- Fix archive conversation when it has scheduled messages
- Fix load conversations when contact is set in url and doesn't exist
- Fix brunch's dialog
- Fix groupon's dialog
- Fix breakfast reservation's dialog
- Fix breakfast refundable's dialog
- Fix concierge's dialog
- Fix delivery's dialog
- Fix airport location's dialog
- Fix number of rooms's dialog
- Fix spoken languages's dialog
- Fix minor child's dialog
- Fix breakfast time's dialog
- Fix discount promo code's dialog
- Fix breakfast menu's dialog
- Fix extra bed's dialog
- Fix baby cot's dialog
- Fix iron in the room's dialog
- Fix safe's dialog
- Fix electric soket adapter's dialog
- Fix coffee nespresso machine's dialog
- Fix mini bar's dialog
- Fix pets's dialog
- Fix swimming pool's dialog
- Fix meeting rooms reservation's dialog
- Fix rooftop reservation's dialog
- Fix send archive conversation by mail
- Fix notification sidebar problem
- Fix tag in archive
- Fix restore and delete actions in archived conversation view

### Removed [4.16.0]

- Remove WhatsApp channel
- Remove unused code
- Remove restrictions on updating live chat languages 
- Remove custom answer from bed size's dialog
- Remove custom answer from shower/bath dialog
- Remove currency from brunch dialog
- Remove duplicate contacts in Quicklist
- Remove 'TripAdvisor' from booking APIs

## [4.15.0] - 2018-09-11

### Added [4.15.0]

- Add user details to livechat endpoint
- Add scroll to tag list
- Add check languages in live chat settings
- Disconnect automaticaly from facebook page when admin is removed by fb user
- Send Email to quicktext operator when disconnect fb page
- Add API to create IM partner
- Add examples to public API doc
- Add custom channel authorize route
- Add verify token to IM partner controller
- Add live chat english language by default
- Add invoice dialog
- Add new message status (failed and resend)

### Changed [4.15.0]

- Update Api routes (remove v1 from prefix)
- Update Brunch dialog
- Update sent messages status
- Optimize Notification system
- Update the sent status icon
- Update fitness gym form
- Update UX contact info
- Update API doc
- Update shower intention
- Update some dependencies (eslint, laravel-mix, vue, cross-env)

### Fixed [4.15.0]

- Fix infinite scroll loader in archive component
- Fix flag icon of phone numbers
- Fix Collapse dropdown in quicklist
- Fix private airport transfer config
- Fix archive message bug
- Fix family rooms dialog
- Fix change bot config command
- Fix partners account API issue for pms connection
- Fix notifcation send mail issue
- Fix notification for new message/conversation
- Fix getMainEmailNotification when notif setting isn't set
- Fix tag in conversation / archived conversation
- Fix buying sms number issue
- Fix sent messages group message archive
- Fix contact info update issue
- Fix access to booking ressource authorisation from api partners
- Fix update messenger config in redis
- Fix update email & phone number of client in real time

### Removed [4.15.0]

- Delete custom answer button  for parking reservation
- Unused node modules
- Remove old custom channel

## [4.14.0] - 2018-08-28

### Added [4.14.0]

- Add last visited page on create new contact
- Add handle user notification
- Add mobile API version
- Add Acccor doc
- Add Acc push new conversation API
- Add CHF currency
- Add desktop notification

### Changed [4.14.0]

- Mobile API versioning
- Enable update status from unreachable to read
- Improve UX
- Update 503 page

### Fixed [4.14.0]

- Fix Bug send message when client does not have number
- Fix bying SMS number
- Fix messages display error
- Fix badge message count
- Fix message counters on delete/archive
- Fix update client in redis

## [4.13.0] - 2018-08-14

### Added [4.13.0]
- Add new channel WhatsApp integration
- Add PMS Sabre/Synxis
- Add PMS Hores
- Add Reusable message item component
- Show custom switcher dynamically using config file
- Add delivery config
- Add sterling pound to Zoe
- Add international format to phone number in modal
- Add german language config
- Add czech language to Zoe config

### Changed [4.13.0]

- Update documentation with new PMS config endpoint
- Update PMS documentation for API Partner
- Update ux in users interface (dashboard)
- Update API partner
- Set custom channel not for prod
- Disable language select on FAQ for non Super Admins
- Enable email notification for more notifications
- Update zoe's name input

### Fixed [4.13.0]

- Fix display duplicated conversation in sent section 
- Fix upload file
- Fix update Zoe setting when no language and no channel is not checked
- Fix conversation's filter
- Fix reception of SMS in Real Time 
- Fix mark as unread message
- Fix display sending SMS using Quicklist 
- Fix scroll to old messages in an archived conversation
- Fix Booking delivery message
- Fix bug viber
- Fix get app call for custom channel
- Fix send messages to BOWO
- Fix uploaded file formats
- Show SMS icon after file message sent
- Fix drag and drop
- Fix Question value of landry Intention
- Fix display right sidebar in sent section

## [4.12.0] - 2018-07-31

### Added [4.12.0]

- Add custom Signature for InterContinental Hotel
- Add scheduled messages delete all button
- Add token generator Helper
- Add API to get booking data
- Add Endpoint to configure PMS via API
- Add notification emails & phones
- Add minor child config
- Add Landry config
- Add PMS Hores
- Add IP restriction when delete phone number
- Add brunch reservation config
- Add brunch reservation notification
- Add parking reservation config
- Add days checklist in restaurant faq intention
- Add airbnb auth
- Add modal to disconnected fb pages contact
- Add validate data
- Add attach/drag&drop file to messages
- Add Super Administrator interface
- Add LC language depend on ZOE Status

### Changed [4.12.0]

- Update documentation with new PMS config endpoint
- Update FAQ config names
- Update documentation at PMS document
- Modify subject of email notification
- Update chatBot config
- Change restaurant intention faq

### Fixed [4.12.0]

- Fix airbnb and custom channel ims
- Fix promo code title config
- Fix unreadCount notification
- Fix getSocket bug
- Fix duplication bookindClientId
- Fix bug of duplicate emails
- Fix display of icons
- Fix special characters's bug
- Fix airbnb msg
- Fix readed msg count at leftSide
- Fix search filter
- Fix last msg timestamp
- Fix multi IM airbnb
- Fix airbnb logo
- Fix email export bug
- Fix get imSettings from parent slot
- Fix filter fb pages in disconnected pages
- Fix obClean on connectIm
- Fix bug of user information's update
- Fix event archive/remove message
- Fix templates's bug
- Fix archived message's interface bug
- Fix airbnb ChannelIds
- Fix display of HTML in archive conversation
- Fix right sidebar in new message interface
- Fix display of right sidebar menu

## [4.11.0] - 2018-07-17

### Added [4.11.0]

- Add 'no availability' notification
- Add country code to clientQt array in sent messages view
- Add hair dryer config
- Add balcony config
- Add rooftop config
- Add rooftop reservation config
- Add custom channel
- Add airbnb channel
- Add dutch language in Zoe

### Changed [4.11.0]

- Update token push notification
- Modify default language in live chat and spoken language in Zoe
- Translate ZOE's greeting message into online mode
- Change tags colors

### Fixed [4.11.0]

- Fix post message job
- Fix dashboard controller
- Correct misspelling
- Fix parse XML File
- Fix tags in archives page
- Fix currency conversion API
- Fix icon sizes
- Fix user info in sent messages
- Send emails for unread messages only when zoe is disabled
- Fix unread messages notification
- Fix duplicated emails in new conversation and new message
- Fix notification emails
- Fix bug affected by check in and check out new format in PMS
- Fix bug display of settings account
- Fix displaying dropdown in pms config
- Improve dashboard UI (remove 'by' in filters)
- Fix password hash when create new user
- Fix display content for image type
- Use static values in move booking logs

## [4.10.0] - 2018-07-03

### Added [4.10.0]
- CS dans dashboard
- Drag & drop image
- Email template
- Render languages spoken config dynamique
- Add tags in archived messages
- [Mobile] Number of unread messages

### Changed [4.10.0]
- UX : settings live chat
- Modify languages spoken config
- Update Avalipro 
- Improve availability indicator
- Connected list
- Diable multiple accounts in HF Hotels

### Fixed [4.10.0]
- Fix phone number format in archived conversation interface
- Date indicator in messages
- Fix reviews bug
- view telephone number
- re-auth messenger on changed password
- bug merge contact
- Button creation of new template HS
- Fix Bug load archived messages
- Bug update contact
- Problem in add members
- Fix Zoe settings page bug

### Removed [4.10.0]
- Remove connexio_Id from booking suite

## [4.9.0] - 2018-06-19

### Added [4.9.0]

- Add Zoe GTM support (new API and UI)
- Restrict conversation visibilty by tags
- Add dossier_Id to Mister Booking API
- Add new FAQ configs
- Add email notification support to Zoe notifications
- Add get messages API to partners APIs
- Add StayNTouch PMS
- Add HotelAppz PMS
- Attach files in QT

### Changed [4.9.0]

- Update Facebook SDK
- Custom functions in quicklist
- Import client name and email from Booking Suite APIs
- Activate zoe's notification in Notification Center
- Change LC default language
- Dynamize spoken language FAQ config

### Fixed [4.9.0]

- Fix bug Quicktext muti-Account
- Fix Rainbow channel
- Fix google places photos
- Fix SMS to Tamise and Brighton
- Fix bug quicklist
- Fix Booking Suite revoke event
- Fix merge contacts bug
- Fix get mobile phone number from PMS

## [4.8.0] - 2018-06-03

## [4.7.0] - 2018-05-22
### Added
- Add bronze plan
- Add webservice to switch accounts
- Add buy SMS pack feature
- Add switch account event call & handle switchedAccount event listen
- Add multi-account notifications
- Add invoices
- Add discount to plans
- Add endpoint to disable twilio account
- Verify SMS count
- Suspend stripe accounts
- New UI to promo code settings
- Add RT event call in chatbot reactivate webjob
- Use new text inputs in private airport transfer
- Booking.com : affect to plan automatically
- Booking.com : suspend account

### Changed
- Move push notifications & billing plans
- Improve FAQ UI/UX
- Improve FAQ multilingual support
- Change default dashboard pagination to 10
- Remove app.js and common.js from version control
- Make sublinks stand in navigation menu
- Code optimization for conversation components 

### Fixed
- Fix mobile API
- Fix booking list, stats, and details in dashboard
- Fix conversation archive command
- Encode booking meta and contact meta values
- Improve navigation menu sublists

## [4.6.0] - 2018-05-08
### Added
- OnBoarding: plan management (feature flagged)
- OnBoarding: stripe webhook (feature flagged)
- OnBoarding: payment via stripe (feature flagged)
- OnBoarding: functionnality restriction following plan (feature flagged)
- Push notification for chrome (feature flagged)
- SSO Login and subscribe with Booking.com
- Archive API endpoint for mobile
- Delete API endpoint for mobile
- Add spanish to FAQ settings

### Changed
- FAQ bot intention
- Update notification user settings
- Reformat mobile API response

### Fixed
- Fix bug booking.com sending message

## [4.5.0] - 2018-04-24
### Added
- User notification settings
- Forward message to partner webhook
- Notification for multi-team
- Live chat client last navigation

### Changed
- Notification system
- FAQ bot intention

### Fixed
- Mobile API
- Bowo connection
- Merge conversation
- Archive conversation channels icons
- Message count in dashboard
- Send message via sms
- Quicklist

### Refactoring
- gulp to webpack
- Vue data-table
- Es-lint

## [4.4.0] - 2018-04-10
### Added
- Admin: listing users
- Admin: assign user to existing account
- Admin: create new account for existing user
- Setup bot actions
- Command: archive conversation
- Command: clear watchdog
- Command: bot re-activation
- Navigation multi-account
- Multiple Twilio integration

### Changed
- Message signature
- Bowo channel
- Bot FAQ

### Fixed
- Live chat connection status
- Send message via sms 

### Refactoring
- Partner API
- Update channel in rt

## [4.3.0] - 2018-03-27
### Added
- PMS api importation Clock
- Manage operator: list, create and disable

### Changed
- PMS api importation Medialog

### Fixed
- Pms import multiple fields
- Live chat mandatory fields
- Account settings
- Merge conversation
- Total sms count on dashboard

### Refactoring
- Update contact info on new message
- FAQ Zoe

## [4.2.0] - 2018-03-13
### Added
- Tag conversation in post message
- Api mobile search by contact name
- Api mobile count total in list messages
- Channel integration booking.com
- Channel integration bowo
- Detail contact in conversation
- Confirm booking process from zoe
- Send email on new notification
- Options for sending emails
- Options on live chat bubble

### Changed
- Sending booking request from Zoe
- Color picker in live chat settings
- Restrict Zoe activation for admin

### Fixed
- Tags in archived conversation
- Faq Zoe translatable field
- Sort filter in dashboard
- Create new team
- New conversation in Facebook
- Hero card message from Facebook
- Mark as read message
- Error 419
- Pms import contact meta
- Email alert on pms importation stats

### Refactoring
- Archive file system on pms import
- Pms import system on multi-field

## [4.1.0] - 2018-02-27
### Added
- Importation method via webhook
- Booking request api & email notification
- Mobile api: SignIn
- Mobile api: Get inbox
- Mobile api: Get sent
- Mobile api: Get conversation message
- Quicktext heath check monitor
- Alert via email on issued importation
- Store booking importation log into secondary database

### Changed
- Live chat contact info form
- Chatbot FAQ setup
- Chatbot name
- Quicklist optimization

### Fixed
- Sent count
- Archive/restore conversation

### Refactoring
- Pms import backlog into (log & backlog)
- Remove deprecated code

## [4.0.0] - 2018-02-03
### Added
- Add new migrations
- Pms import backlog
- Admin teams views
- Admin bookings views
- Admin bookings stats views
- Admin logs views
- Table foreign key

### Changed
- Message system
- Table schema: contact, message
- Migrate data: from booking meta to contact meta
- Bot faq

### Fixed
- Message issue: send, receive, group
- Merge conversation

## [4.0.0-rc1] - 2018-02-02
### Added
- Add new migrations

## [3.1.0] - 2017-12-25
### Added
- Api mobile authenticate endpoint
- Api mobile revoke token endpoint
- Api mobile check token endpoint
- Livechat translations

### Changed
- Chat bot setup when not activated
- Refactoring php code

### Fixed
- Store group message

## [3.1.0-rc3] - 2017-11-27
### Added
- Add new notification

### Changed
- Chat bot image

### Fixed
- Availpro api
- LC unknown user image
- Socket reconnect

## [3.1.0-rc2] - 2017-11-24
### Added
- Add new notification

### Changed
- Use single night price in availpro
- Notification audio
- Livechat settings data

### Fixed
- Notification
- Disable bot in conversation
- Chatbot FAQ service settings
- Edit contact (+realtime)
- Update contact name
- Admin views filters
- Reconnect client socket

### Removed
- Unused libs

## [3.1.0-rc1] - 2017-11-21
### Added
- Admin bookings importation stats view

### Changed
- Admin team view
- Admin watchdog view

### Fixed
- API: Messages bulk saves
- API: Client engage

### Removed
- Unused libs
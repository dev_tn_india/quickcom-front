<?php

namespace Tests\Unit;

use App\Team;
use App\User;
use Tests\TestCase;

class ContactsTest extends TestCase
{
	/**
     * A basic test example.
     *
     * @return void
     */
    public function test_contact_create()
    {
        $user = User::create([
            'name' => 'TEST USER',
            'last_name' => 'LN',
            'email' => 'tu@xx.com',
            'password' => md5('6543210'),
            'is_admin' => false,
            'new_user' => true,
            'current_team_id' => null
        ]);

		/** @var Team $team */
		$team = Team::create([ 'name' => 'TEST_TEAM', 'slug' => 's_g', 'bot_name' => 't', 'owner_id' => $user->id ]);
        $team->users()->attach($user->id);

        $user->current_team_id = $team->id;

        $this->assertEquals('TEST USER', $user->name);
        $this->assertEquals(1, $user->id);
        $this->assertEquals(1, $team->users()->count());
        $this->assertEquals($team->id, $user->current_team_id);

    }
}

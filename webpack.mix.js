const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.
	js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/main.js', 'public/js')
	.sass('resources/assets/sass/main.scss', 'public/css')
	.copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
	.options({
		processCssUrls: false,
	})
	.then(() => { })
	.webpackConfig({
		resolve: {
			modules: [
				path.resolve(__dirname, 'resources/assets/js/spark'),
				'node_modules',
			],
			alias: {
				'vue$': 'vue/dist/vue.js',
			},
		},
	})
	.js('resources/assets/js/components/account/main.js', 'public/js/components/account/main.js')
	.js('resources/assets/js/components/chartBooking/main.js', 'public/js/components/chartBooking/chartBooking.js')
	.js('resources/assets/js/components/compose/main.js', 'public/js/components/compose/compose.js')
	.js('resources/assets/js/components/chartBooking/StatBookingImport.js', 'public/js/components/chartBooking/statistique.js')
	.js('resources/assets/js/components/conversation/conversationApp.js', 'public/js/components/conversation/conversation.js')
	.js('resources/assets/js/components/archivedConversation/main.js', 'public/js/components/archivedConversation/archivedConversation.js')
	.js('resources/assets/js/components/settings/settings.js', 'public/js/components/settings/settings.js')
	.js('resources/assets/js/components/contacts/main.js', 'public/js/components/contacts/contacts.js')
	.js('resources/assets/js/components/contactLists/main.js', 'public/js/components/contactLists/contact-lists.js')
	.js('resources/assets/js/components/scheduled/main.js', 'public/js/components/scheduled/scheduled.js')
	.js('resources/assets/js/components/scripts/main.js', 'public/js/components/scripts/scripts.js')
	.js('resources/assets/js/components/scriptLists/main.js', 'public/js/components/scriptLists/scriptLists.js')
	.js('resources/assets/js/components/drafts/main.js', 'public/js/components/drafts/drafts.js')
	.js('resources/assets/js/components/templates/main.js', 'public/js/components/templates/templates.js')
	.js('resources/assets/js/components/blacklist/main.js', 'public/js/components/blacklist/blacklist.js')
	.js('resources/assets/js/components/dashboard/main.js', 'public/js/components/dashboard/dashboard.js')
	.js('resources/assets/js/components/watchdog/main.js', 'public/js/components/watchdog/watchdog.js')
	.js('resources/assets/js/components/team/main.js', 'public/js/components/team/team.js')
	.js('resources/assets/js/components/subscriptions/main.js', 'public/js/components/subscriptions/subscriptions.js')
	.js('resources/assets/js/components/teamProfile/main.js', 'public/js/components/team/team_profile.js')
	.js('resources/assets/js/components/tags/main.js', 'public/js/components/tags/tags.js')
	.js('resources/assets/js/components/booking/table.js', 'public/js/components/booking/booking.js')
	.js('resources/assets/js/components/templateCategories/main.js', 'public/js/components/template-categories/categories.js')
	.js('resources/assets/js/components/chatBot/main.js', 'public/js/components/chat-bot/chat-bot.js')
	.js('resources/assets/js/components/liveChat/main.js', 'public/js/components/lc-settings/lc-settings.js')
	.js('resources/assets/js/components/notification/main.js', 'public/js/components/notification/notification.js')
	.js('resources/assets/js/components/notification/notificationHandler.js', 'public/js/components/notification/notification-handler.js')
	.js('resources/assets/js/components/notification/notificationHack.js', 'public/js/components/notification/notification-hack.js')
	.js('resources/assets/js/components/teamHosts/main.js', 'public/js/components/teamHosts/team_hosts.js')
	.js('resources/assets/js/components/healthCheck/main.js', 'public/js/components/healthCheck/health_check.js')
	.js('resources/assets/js/components/pms/main.js', 'public/js/components/pms/pms.js')
	.js('resources/assets/js/components/teamMembers/main.js', 'public/js/components/teamMembers/team-members.js')
	.js('resources/assets/js/components/users/main.js', 'public/js/components/users/team-users.js')
	.js('resources/assets/js/components/notifications-center/main.js', 'public/js/components/notifications-center/notificationsCenter.js')
	.js('resources/assets/js/components/onboarding/main.js', 'public/js/components/onboarding/onboarding.js')
	.js('resources/assets/js/bootstrap.js', 'public/js/common.js')
	.js('resources/assets/js/swaggerDocs/PMAPI.js', 'public/docs/PMAPI/index.js')
	.version([
		'public/css/main.css',
		'public/css/dashboard.css',
		'public/js/components/account/main.js',
		'public/js/components/chartBooking/chartBooking.js',
		'public/js/components/chartBooking/statistique.js',
		'public/js/components/compose/compose.js',
		'public/js/components/conversation/conversation.js',
		'public/js/components/archivedConversation/archivedConversation.js',
		'public/js/components/settings/settings.js',
		'public/js/components/contacts/contacts.js',
		'public/js/components/contactLists/contact-lists.js',
		'public/js/components/scheduled/scheduled.js',
		'public/js/components/scripts/scripts.js',
		'public/js/components/scriptLists/scriptLists.js',
		'public/js/components/drafts/drafts.js',
		'public/js/components/templates/templates.js',
		'public/js/components/blacklist/blacklist.js',
		'public/js/components/dashboard/dashboard.js',
		'public/js/components/watchdog/watchdog.js',
		'public/js/components/team/team.js',
		'public/js/components/subscriptions/subscriptions.js',
		'public/js/components/team/team_profile.js',
		'public/js/components/tags/tags.js',
		'public/js/components/booking/booking.js',
		'public/js/components/template-categories/categories.js',
		'public/js/components/chat-bot/chat-bot.js',
		'public/js/components/lc-settings/lc-settings.js',
		'public/js/components/notification/notification.js',
		'public/js/components/notification/notification-handler.js',
		'public/js/components/notification/notification-hack.js',
		'public/js/components/teamHosts/team_hosts.js',
		'public/js/components/pms/pms.js',
		'public/js/components/healthCheck/health_check.js',
		'public/js/components/teamMembers/team-members.js',
		'public/js/components/users/team-users.js',
		'public/js/components/notifications-center/notificationsCenter.js',
		'public/js/components/onboarding/onboarding.js',
		'public/js/common.js',
		'public/docs/PMAPI/index.js',
	]);
